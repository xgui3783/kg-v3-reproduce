#! /bin/bash

QUERY_ID="bebbe365-a0d6-41ea-9ff8-2554c15f70b7"
INSTANCE_ID="0f1ccc4a-9a11-4697-b43f-9c9c8ac543e6"
URL="https://core.kg.ebrains.eu/v3-beta/queries/$QUERY_ID/instances?stage=RELEASED&dataset_id=$INSTANCE_ID"
TOKEN_CI="ey..."
TOKEN_PSWD="ey..."
curl -vH "Authorization: Bearer $TOKEN_PSWD" $URL | jq '.data[0].versions | map(.files | length)'
# returns [21, 22]
curl -vH "Authorization: Bearer $TOKEN_CI" $URL | jq '.data[0].versions | map(.files | length)'
# returns [0, 0]